﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using ClassLibrary;
using Microsoft.Win32;
using System.Xml.Serialization;

namespace RoN_Town_Simulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const double FREELANCE_EARNING = 0.1863 * 0.05; // weighted average total earnings for a plot / the share our freelancers are likely to earn
        const int STARTING_BP = 0;
        const int MAX_SIM_WEEKS = 52;
        List<Plot> PlotsAvailable;
        ObservableCollection<PlotList> CurrentWeekPlotsConstructed;
        ObservableCollection<PlotList> CurrentWeekPlotsUnderConstruction;
        public ObservableCollection<Town> History = new ObservableCollection<Town>();
        public TownSeed activeTownSeed;


        // Town town = new Town();    WE DON'T NEED THIS, WE STORE HISTORY, NOT "CURRENT" TOWN

        public MainWindow()
        {
            InitializeComponent();
            PlotsAvailable = PlotService.ReadFile(@"plots.csv");
            DataContext = PlotsAvailable;
 //           bool townChanged = false;
            dg_Town_Status.ItemsSource = History;
            
      //      dg_PlotsUnderConstruction.ItemsSource = CurrentWeekPlotsUnderConstruction;

            //    RaisePropertyChanged("History");
        }

        private void Btn_LoadPlots_Click(object sender, RoutedEventArgs e)
        {
            AddPlotToPriorityList();
        }

        private void Lb_Plots_Available_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void Dg_Town_Status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //     MessageBox.Show("You Selected row " + dg_Town_Status.SelectedIndex);
            if (dg_Town_Status.SelectedIndex >= 0)
            {
                lbl_WeekNum.Content = "Week " + dg_Town_Status.SelectedIndex;
                // CurrentWeekPlotsConstructed.Clear();
                CurrentWeekPlotsConstructed = History[dg_Town_Status.SelectedIndex].BuiltPlotsList;
                dg_BuiltPlots.ItemsSource = CurrentWeekPlotsConstructed;
            }
            else
                CurrentWeekPlotsConstructed.Clear();



        }

        private void Btn_RemovePlot_Click(object sender, RoutedEventArgs e)
        {
            // int plotIndex = lb_Build_Priority.SelectedIndex;
            lb_Build_Priority.Items.RemoveAt(lb_Build_Priority.SelectedIndex);
    
            CalculateWeeks();
        }

        private void Btn_SaveTown_Click(object sender, RoutedEventArgs e)
        {
            SaveTown();
        }

    private void CalculateWeeks()
        {
            Plot plotData;
            int currentWeekNum = 0;
            bool doneBuildingThisWeek = false;
            Town town;
            // town = new Town(); // later see if we want to save the old state for comparison
            History.Clear(); // ditto

            /*
             * [Future idea: instead of referencing the static "PlotsAvailable," make a copy. THen events can change properties over time. E.g. Certain building makes more income, costs extra, takes less turns to build, etc.
             * 
             * Stubbing this out
             * First need a textbox to capture the starting BP (later can import a saved "town" object to start with an established town)
             * Assume "week 0" is the starting state.
             * set current week = 0
             * Make a new week (int CurrentWeek+1, currentWeek.town) (this initilizes the new week with the stats of the current one)
             * Check if we have enough BP to build the current priority. If so, 
             * 
             * Subtract the cost from current BP
             * set town production to sum of each plot production
             * same for Mx
             * Add production to BP
             * Subtract Mx from BP
             * - Add item to "under construction list" (need to make this. Simple list of name, turns remaining)
             * - - Decrement turns remaining of existing construction
             * - - If turns remaining = 0, add matching item to plots
             * 
             * [later add a repair list]
             // also later, if you need this, double total = myList.Where(item => item.Name == "Eggs").Sum(item => item.Amount);
             // (I was thinking I could use this to sum influence types once I code them)
             // https://stackoverflow.com/questions/4351876/c-sharp-list-of-objects-how-do-i-get-the-sum-of-a-property
             // Looks like using the foreach loop is much faster than linq shortcut though so that's what I use below.
             
             */

            History.Add(new Town());
            History[0].BuildPointsAvailable = STARTING_BP;
            History[0].Population = int.Parse(tbx_StartingPopulation.Text);
            History[0].WeekNumber = currentWeekNum;
            List<string> BuildQueue = new List<string>();
            BuildQueue = lb_Build_Priority.Items.Cast<String>().ToList();

            //          foreach (var buildItem in lb_Build_Priority.Items)
            //        {
            town = History[currentWeekNum]; // makes things easier

            // bool builtSomething = false;

            // *** Start new construction ***

            while (((BuildQueue.Count > 0) || (town.HasQueue())) && (currentWeekNum < MAX_SIM_WEEKS)) // do until no more constuction is or will happen or we run out of week
            {
                // *** Establish new week and town status ***
                //    

                History.Add(new Town(History[currentWeekNum]));
                currentWeekNum++; // building the new week, updates CurretnTown
                town = History[currentWeekNum]; // makes things easier



                // *** Start construction ***
                //     

                doneBuildingThisWeek = false;

                // Get the next item from the build queue
                while (BuildQueue.Count > 0 && !(doneBuildingThisWeek)) // spend all our money
                {
                    plotData = PlotsAvailable.Where(i => i.Name == BuildQueue[0].ToString()).FirstOrDefault();

                    //Build it if we can afford it:
                    if (plotData.BuildPointsCost <= town.BuildPointsAvailable)
                    {
                        town.BuildPointsAvailable -= plotData.BuildPointsCost; // Buy it
                        // put into the construction queue
                        town.Queue(new Plot(plotData));
                        BuildQueue.Remove(BuildQueue[0]);
                        //BuildQueue.RemoveRange(0,1); // take this off the todo list
                    }
                    else
                    {
                        doneBuildingThisWeek = true; // don't try to build until we get more money
                    }

                }




                // *** Generate money ***
                //          
            
                town.BuildPointsAvailable = town.BuildPointsAvailable + town.WeeklyProduction - town.WeeklyMaintenance + 
                    (town.Population - town.PeopleEmployed) * FREELANCE_EARNING;



                // MessageBox.Show(plotData.Name);
                town.Build();



                // Check the construction queue for completed 

            }
            //      }
            //             MessageBox.Show("Tada");
        }

        private void SaveTown()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TownSeed));
            activeTownSeed = new TownSeed(lb_Build_Priority.Items.Cast<String>().ToList(), tbx_TownName.Text, int.Parse(tbx_StartingPopulation.Text));
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Rise of Netheril Town Files (*.twn)|*.twn";
            saveFileDialog.DefaultExt = "twn";
            saveFileDialog.AddExtension = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string filename = saveFileDialog.FileName;
         //       MessageBox.Show("You saved the file " + filename);

                // Create the TextWriter for the serialiser to use
                TextWriter filestream = new StreamWriter(filename);

                //write to the file
                serializer.Serialize(filestream, activeTownSeed);
           
                // Close the file
                filestream.Close();

                mainWindow.Title = "Rise Of Netheril Town Sim - " + filename;
                // https://stackoverflow.com/questions/8334527/save-listt-to-xml-file         
                // https://docs.microsoft.com/en-us/dotnet/standard/serialization/examples-of-xml-serialization
                // https://docs.microsoft.com/en-us/dotnet/standard/serialization/introducing-xml-serialization
            }

            // https://www.wpf-tutorial.com/dialogs/the-savefiledialog/


            // 

            /*  const string sPath = "save.txt";

  System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(sPath);
  SaveFile.WriteLine(lb_Build_Priority.Items);
  SaveFile.ToString();
  SaveFile.Close();
  */
        }


        private void LoadTown()
        {



            XmlSerializer serializer = new XmlSerializer(typeof(TownSeed));
            TownSeed activeTownSeed;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Rise of Netheril Town Files (*.twn)|*.twn";
            if (openFileDialog.ShowDialog() == true)

            {
                // MessageBox.Show("You loaded the file " + openFileDialog.FileName);
                string filename = openFileDialog.FileName;
                //       MessageBox.Show("You saved the file " + filename);

                // Create the TextWriter for the serialiser to use
                StreamReader filestream = new StreamReader(filename);

                //Read from the file

                activeTownSeed = (TownSeed)serializer.Deserialize(filestream);
                // Close the file
                filestream.Close();

                lb_Build_Priority.Items.Clear();
                foreach (string s in activeTownSeed.PriorityList)
                {
                    lb_Build_Priority.Items.Add(s);
                }

                mainWindow.Title = "Rise Of Netheril Town Sim - " + filename;
                //             townChanged = false;
                // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/serialization/how-to-read-object-data-from-an-xml-file
                // Above has an error: better refernce: https://docs.microsoft.com/en-us/dotnet/api/system.xml.serialization.xmlserializer.deserialize?view=netframework-4.8

                tbx_TownName.Text = activeTownSeed.TownName;
                tbx_StartingPopulation.Text = activeTownSeed.StartingPopulation.ToString();
            }


        }
        private void AddPlotToPriorityList()
        {
            int plotIndex = lb_Plots_Available.SelectedIndex;
            if ((plotIndex > -1) && (plotIndex < PlotsAvailable.Count))
            {
                // MessageBox.Show(PlotsAvailable[plotIndex].Name);
                lb_Build_Priority.Items.Add(PlotsAvailable[plotIndex].Name);
  //              townChanged = true;
            }
            CalculateWeeks();
        }

        private void Btn_LoadTown_Click(object sender, RoutedEventArgs e)
        {
            LoadTown();
            CalculateWeeks();
        }

        private void Lb_Plots_Available_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AddPlotToPriorityList();
        }

        private void Tbx_StartingPopulation_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(tbx_StartingPopulation.Text, out int result))
            {
                CalculateWeeks();
            }
            else
                MessageBox.Show("Please type an integer");
        }

        private void Btn_MovePlotUp_Click(object sender, RoutedEventArgs e)
        {
            MoveItem(-1, lb_Build_Priority);
            CalculateWeeks();

            // https://stackoverflow.com/questions/4796109/how-to-move-item-in-listbox-up-and-down
        }
        private void Btn_MovePlotDown_Click(object sender, RoutedEventArgs e)
        {
            MoveItem(1, lb_Build_Priority);
            CalculateWeeks();
        }

        private void MoveItem(int direction, ListBox lb)
        {
            // Checking selected item
            if (lb.SelectedItem == null || lb.SelectedIndex < 0)
                return; // No selected item - nothing to do

            // Calculate new index using move direction
            int newIndex = lb.SelectedIndex + direction;

            // Checking bounds of the range
            if (newIndex < 0 || newIndex >= lb.Items.Count)
                return; // Index out of range - nothing to do

            object selected = lb.SelectedItem;

            // Removing removable element
            // lb.Items.Remove(selected); This doesn't work with duplicate named items (it just removes the first matching text)
            lb.Items.RemoveAt(lb.SelectedIndex);
            // Insert it in new position
            lb.Items.Insert(newIndex, selected);
            // Restore selection
            lb.SelectedIndex = newIndex;
        }


    }




    /* public class Week : INotifyPropertyChanged
     {
         // This seems redundant since I could just use the index of the list as the "number." but I don't
         // like depending on the list order, and I think other metadata for each week may be useful in the future
         // (e.g. description of any events that happen, or a record of events that affect the town).
         public int Number { get; set; }

         public Town town { get; set; }

         public Week()
         {
             Number = 0;
             town = new Town();
         }
              public Week(Week week)
         {
             Number = week.Number+1;
             town = new Town(week.town);
         }

         /*      public event PropertyChangedEventHandler PropertyChanged;

               private void RaisePropertyChanged(string propName)
               {
                   PropertyChanged(this, new PropertyChangedEventArgs(propName));
               }*/
    // Add more later



    /*      public class ConstructionPlot  - this was redundant since I can just use the 
     *      "construction time" field to track weeks left to build (thought of this when I 
     *      accidentally decremented that field instead of "weeksUntilCompletion"
      {


          public Plot plotUnderConstruction { get; set; }

          public ConstructionPlot(Plot referencePlot)
          {
              weeksUntilCompletion = referencePlot.ConstructionTime;
              plotUnderConstruction = new Plot(referencePlot);
          }

          // Add more later

      }
      */
    public class PlotService
    {
        // Borrowed the algorithm from this post: https://stackoverflow.com/questions/20574464/csv-text-in-datagrid-wpf
        public static List<Plot> ReadFile(string filepath)
        {
            var lines = File.ReadAllLines(filepath);

            var data = from l in lines.Skip(0)
                       let split = l.Split(',')
                       select new Plot
                       {
                           Name = split[0],
                           BuildPointsCost = int.Parse(split[1]),
                           WeeklyMaintenance = double.Parse(split[2]),
                           WeeklyProduction = int.Parse(split[3]),
                           PeopleEmployed = int.Parse(split[4]),
                           InfluencePoints = int.Parse(split[5]),
                           RepairCost = double.Parse(split[6]),
                           ConstructionTime = int.Parse(split[7])
                       };

            return data.ToList();
        }

        [Serializable()]
        public class TownSeed
        { 
       
       
        }


    }
}



