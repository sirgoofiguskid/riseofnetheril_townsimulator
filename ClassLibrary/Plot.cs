﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
    public class Plot
    {
        public string Name { get; set; }  // The name of the plot type

        public int BuildPointsCost { get; set; } 
        /* The cost to construct this plot in the "Build Point" currency.
         * The source uses three currencies: gold, resource units (representing 
         * goods that are prodcued and sold), and build points (representing the 
         * costs to construct a new plot). For simplicity I converted all table 
         * values to the "build point" currency. The conversion ratios are
         * 1 BP = 10 resource points = 1000 gp.
            
         */
        public double WeeklyMaintenance { get; set; } 
        /* Represents the costs to operate and maintain the plot. In theory you 
         * can not pay the Mx cost but each week you do this you incrue a risk of
         * something bad happening, including falling into disrepair. */

        public double WeeklyProduction { get; set; } // How much BP the active plot produces each week.
        // TODO: Add "operational status" property and only generate production if it is operational.

        public int PeopleEmployed { get; set; } 
        /* How many laborers are employed. In our game, we h
        * in our game, we have something like  360 people, but I need to check my reference. We 
        rolled this number at the end of the last session. */

        public int InfluencePoints { get; set; }
        /* This is a simplification of the source's system. There are multiple types of influence and
         * the more you have of one the less likely bad things are to happen. I figured most of those
         * wouldn't map well to this timeline and wanted to keep things simple (especially for plots
         * that generate more than one type of influence). But the general idea is the more security
         * influence I have the less likely I am to get conquered, the more comfort influence, I'll
         * attract higher classes of people, health I'm less likely to get deseased, etc. I don't 
         * want to mess with that complexity right now so we'll just use this as an overall measure
         * of town strength. */

        public double RepairCost { get; set; }
        /* if this plot becomes damaged for any reason, it costs this much BP to fix it. */

        public int ConstructionTime { get; set; }
        /* Number of time (in weeks) it takes to construct the plot. Ideally the income calculations 
         * refelect this accurately. E.g. 0 weeks is an instant build so you should get income from
         * it in that week. */


        public Plot()
        {
            Name = "";
            BuildPointsCost = 0;
            WeeklyMaintenance = 0;
            WeeklyProduction = 0;
            PeopleEmployed = 0;
            InfluencePoints = 0;
            RepairCost = 0;
            ConstructionTime = 0;
        }

        public Plot(Plot plot)
        {
            Name = plot.Name;
            BuildPointsCost = plot.BuildPointsCost;
            WeeklyMaintenance = plot.WeeklyMaintenance;
            WeeklyProduction = plot.WeeklyProduction;
            PeopleEmployed = plot.PeopleEmployed;
            InfluencePoints = plot.InfluencePoints;
            RepairCost = plot.RepairCost;
            ConstructionTime = plot.ConstructionTime;
        }

        // Add more later

    }    

    public class PlotList
    {
        public string Name { get; set; }  // The name of the plot type

        public int Qty { get; set; }

        public PlotList()
        { }

        public PlotList(string name, int qty)
        {
            this.Name = name;
            this.Qty = qty;
        }

    }
}
