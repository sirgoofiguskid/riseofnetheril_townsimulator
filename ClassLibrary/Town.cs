﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;


namespace ClassLibrary
{
    public class Town
    {
        public int WeekNumber { get; set; } // The index of the current week (week 0 representing the initial state)

        public double BuildPointsAvailable { get; set; } // The total BP available to the town as a whole (no RP restrictions implemented)

        public double WeeklyMaintenance { get; set; } // The total maintenance cost (in BP) of all plots in the town

        public double WeeklyProduction { get; set; } // The total BP produced by all plots in the town

        public int PeopleEmployed { get; set; } // The total labor pool consumed by the town

        public int Population { get; set; }

        public int InfluencePoints { get; set; } // The total influence points contributed by the plots in the town

        public int Built { get { return this.Plots.Count(); } } // Public handle for the plots that are constructed [and operational] in the town

        public int Queued { get { return this.PlotsUnderConstruction.Count(); } } // A list of the plots currently being constructed

        public ObservableCollection<PlotList> BuiltPlotsList { get {
                List<String> list = new List<String>();
                ObservableCollection<PlotList> plist = new ObservableCollection<PlotList>();
                foreach (Plot p in this.Plots)
                {
                    if (list.Contains(p.Name))
                    {
                        int i = list.IndexOf(p.Name.ToString());
                        plist[i].Qty++;
                    }
                    else
                    {
                        list.Add(p.Name);
                        int i = list.IndexOf(p.Name);
                        plist.Insert(i, new PlotList(p.Name, 1));
                    }
                }
                return plist;
            }
        }
        //       foreach (listBoxItem in )



        internal List<Plot> Plots { get; set; } = new List<Plot>(); // internal "Built" list

        internal List<Plot> PlotsUnderConstruction { get; set; } = new List<Plot>(); // internal "Queued" list
        // Random note: should the model be improved to include construction labor? or should we just assume the workers build it so they need to be available

        public Town()
        {
        }

        public Town(Town town)
        {
            WeekNumber = town.WeekNumber + 1;
            BuildPointsAvailable = town.BuildPointsAvailable;
            WeeklyMaintenance = town.WeeklyMaintenance;
            WeeklyProduction = town.WeeklyProduction;
            PeopleEmployed = town.PeopleEmployed;
            Population = town.Population;
            InfluencePoints = town.InfluencePoints;
            Plots.AddRange(town.Plots);
            PlotsUnderConstruction.AddRange(town.PlotsUnderConstruction);
        }

        public void Build()
        {
            // Continue Construction
            for (int i = 0; i < this.PlotsUnderConstruction.Count; i++)
            {
                this.PlotsUnderConstruction[i].ConstructionTime--;
                if (this.PlotsUnderConstruction[i].ConstructionTime <= 0)
                {
                    this.Plots.Add(this.PlotsUnderConstruction[i]);

                    this.InfluencePoints += this.PlotsUnderConstruction[i].InfluencePoints;
                    this.PeopleEmployed += this.PlotsUnderConstruction[i].PeopleEmployed;
                    this.WeeklyMaintenance += this.PlotsUnderConstruction[i].WeeklyMaintenance;
                    this.WeeklyProduction += this.PlotsUnderConstruction[i].WeeklyProduction;

                    this.PlotsUnderConstruction.Remove(this.PlotsUnderConstruction[i]);
                }
            }
        }

        public bool HasQueue()
        {
            return this.PlotsUnderConstruction.Any();
        }

        public void Queue(Plot plot)
        {
            this.PlotsUnderConstruction.Add(plot);
        }

    }

   // [Serializable]
    public class TownSeed   // Collects data needed to save and load a town
    {
        public List<String> PriorityList { get; set; } // The index of the current week (week 0 representing the initial state)
        public string TownName { get; set; }

        public int StartingPopulation { get; set; }
        public TownSeed()
        {
            // Necessary for the serializer
        }
        public TownSeed(List<String> lb_list, string lb_name, int lb_pop)
        {
            PriorityList = lb_list;
            TownName = lb_name;
            StartingPopulation = lb_pop;
        }


    
    

    }
}
